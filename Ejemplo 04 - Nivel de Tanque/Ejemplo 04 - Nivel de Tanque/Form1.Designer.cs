﻿
namespace Ejemplo_04___Nivel_de_Tanque
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panelTanque = new System.Windows.Forms.Panel();
            this.panelLíquido = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnLlenar = new System.Windows.Forms.Button();
            this.btnVaciar = new System.Windows.Forms.Button();
            this.tmrLlenado = new System.Windows.Forms.Timer(this.components);
            this.tmrVaciado = new System.Windows.Forms.Timer(this.components);
            this.tmrVariables = new System.Windows.Forms.Timer(this.components);
            this.pbxLlenado = new System.Windows.Forms.PictureBox();
            this.pbxVaciado = new System.Windows.Forms.PictureBox();
            this.btnParo = new System.Windows.Forms.Button();
            this.pbxParo = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.panelTanque.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxLlenado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxVaciado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxParo)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel1.Controls.Add(this.panelTanque);
            this.panel1.Location = new System.Drawing.Point(184, 114);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(4);
            this.panel1.Size = new System.Drawing.Size(24, 196);
            this.panel1.TabIndex = 0;
            // 
            // panelTanque
            // 
            this.panelTanque.BackColor = System.Drawing.Color.White;
            this.panelTanque.Controls.Add(this.panelLíquido);
            this.panelTanque.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelTanque.Location = new System.Drawing.Point(4, 4);
            this.panelTanque.Name = "panelTanque";
            this.panelTanque.Size = new System.Drawing.Size(16, 188);
            this.panelTanque.TabIndex = 0;
            // 
            // panelLíquido
            // 
            this.panelLíquido.BackColor = System.Drawing.Color.MediumBlue;
            this.panelLíquido.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelLíquido.Location = new System.Drawing.Point(0, 104);
            this.panelLíquido.Name = "panelLíquido";
            this.panelLíquido.Size = new System.Drawing.Size(16, 84);
            this.panelLíquido.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Ejemplo_04___Nivel_de_Tanque.Properties.Resources.Recurso_1tank2;
            this.pictureBox1.Location = new System.Drawing.Point(158, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(398, 329);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.DarkGray;
            this.panel4.Controls.Add(this.pbxParo);
            this.panel4.Controls.Add(this.pbxVaciado);
            this.panel4.Controls.Add(this.pbxLlenado);
            this.panel4.Controls.Add(this.btnParo);
            this.panel4.Controls.Add(this.btnVaciar);
            this.panel4.Controls.Add(this.btnLlenar);
            this.panel4.Location = new System.Drawing.Point(7, 87);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(129, 155);
            this.panel4.TabIndex = 2;
            // 
            // btnLlenar
            // 
            this.btnLlenar.Location = new System.Drawing.Point(13, 19);
            this.btnLlenar.Name = "btnLlenar";
            this.btnLlenar.Size = new System.Drawing.Size(69, 26);
            this.btnLlenar.TabIndex = 0;
            this.btnLlenar.Text = "Llenar";
            this.btnLlenar.UseVisualStyleBackColor = true;
            this.btnLlenar.Click += new System.EventHandler(this.btnLlenar_Click);
            // 
            // btnVaciar
            // 
            this.btnVaciar.Location = new System.Drawing.Point(13, 63);
            this.btnVaciar.Name = "btnVaciar";
            this.btnVaciar.Size = new System.Drawing.Size(69, 26);
            this.btnVaciar.TabIndex = 0;
            this.btnVaciar.Text = "Vaciado";
            this.btnVaciar.UseVisualStyleBackColor = true;
            this.btnVaciar.Click += new System.EventHandler(this.btnVaciar_Click);
            // 
            // tmrLlenado
            // 
            this.tmrLlenado.Interval = 50;
            this.tmrLlenado.Tick += new System.EventHandler(this.tmrLlenado_Tick);
            // 
            // tmrVaciado
            // 
            this.tmrVaciado.Interval = 50;
            this.tmrVaciado.Tick += new System.EventHandler(this.tmrVaciado_Tick);
            // 
            // tmrVariables
            // 
            this.tmrVariables.Enabled = true;
            this.tmrVariables.Interval = 500;
            this.tmrVariables.Tick += new System.EventHandler(this.tmrVariables_Tick);
            // 
            // pbxLlenado
            // 
            this.pbxLlenado.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(32)))), ((int)(((byte)(0)))));
            this.pbxLlenado.Location = new System.Drawing.Point(89, 19);
            this.pbxLlenado.Name = "pbxLlenado";
            this.pbxLlenado.Size = new System.Drawing.Size(27, 26);
            this.pbxLlenado.TabIndex = 1;
            this.pbxLlenado.TabStop = false;
            // 
            // pbxVaciado
            // 
            this.pbxVaciado.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(32)))), ((int)(((byte)(0)))));
            this.pbxVaciado.Location = new System.Drawing.Point(88, 63);
            this.pbxVaciado.Name = "pbxVaciado";
            this.pbxVaciado.Size = new System.Drawing.Size(27, 26);
            this.pbxVaciado.TabIndex = 1;
            this.pbxVaciado.TabStop = false;
            // 
            // btnParo
            // 
            this.btnParo.Location = new System.Drawing.Point(13, 104);
            this.btnParo.Name = "btnParo";
            this.btnParo.Size = new System.Drawing.Size(69, 26);
            this.btnParo.TabIndex = 0;
            this.btnParo.Text = "Paro";
            this.btnParo.UseVisualStyleBackColor = true;
            this.btnParo.Click += new System.EventHandler(this.btnParo_Click);
            // 
            // pbxParo
            // 
            this.pbxParo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pbxParo.Location = new System.Drawing.Point(88, 104);
            this.pbxParo.Name = "pbxParo";
            this.pbxParo.Size = new System.Drawing.Size(27, 26);
            this.pbxParo.TabIndex = 1;
            this.pbxParo.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(608, 356);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.Text = "Simulacion de control de Nivel";
            this.panel1.ResumeLayout(false);
            this.panelTanque.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbxLlenado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxVaciado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxParo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panelTanque;
        private System.Windows.Forms.Panel panelLíquido;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnVaciar;
        private System.Windows.Forms.Button btnLlenar;
        private System.Windows.Forms.Timer tmrLlenado;
        private System.Windows.Forms.Timer tmrVaciado;
        private System.Windows.Forms.Timer tmrVariables;
        private System.Windows.Forms.PictureBox pbxVaciado;
        private System.Windows.Forms.PictureBox pbxLlenado;
        private System.Windows.Forms.PictureBox pbxParo;
        private System.Windows.Forms.Button btnParo;
    }
}

